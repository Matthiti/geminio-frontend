module.exports = {
  css: {
    extract: true,
    loaderOptions: {
      sass: {
        prependData: '@import "@/assets/scss/_variables.scss";'
      }
    }
  },
  devServer: {
    proxy: {
      "^/api": {
        target: "http://localhost:5000",
        changeOrigin: true
      }
    }
  }
};
