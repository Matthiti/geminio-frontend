function urlOf(file) {
  const port = window.location.port ? `:${window.location.port}` : '';
  return `${window.location.protocol}//${window.location.hostname}${port}/file/${file.slug}`;
}

export default {
  urlOf
}
